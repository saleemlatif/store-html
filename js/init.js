(function($){
  $(function(){

    $('.sidenav').sidenav();

    const instance = M.Carousel.init({
      fullWidth: true,
      indicators: true
    });

    // Or with jQuery

    $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: false
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space
